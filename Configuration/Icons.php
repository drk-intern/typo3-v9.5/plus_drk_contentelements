<?php
/**
 * https://docs.typo3.org/m/typo3/reference-coreapi/main/en-us/ExtensionArchitecture/FileStructure/Configuration/Icons.html
 */

return [
    'icon-plusdrkcontentelements-donation-info' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:plus_drk_contentelements/Resources/Public/Icons/FontAwesome/info.svg'
    ],
    'icon-plusdrkcontentelements-facts-tiles' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:plus_drk_contentelements/Resources/Public/Icons/FontAwesome/th-list.svg'
    ],
    'icon-plusdrkcontentelements-icon-tiles' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:plus_drk_contentelements/Resources/Public/Icons/FontAwesome/cubes.svg'
    ],
    'icon-plusdrkcontentelements-multimedia-header' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:plus_drk_contentelements/Resources/Public/Icons/FontAwesome/image.svg'
    ],
    'icon-plusdrkcontentelements-project-partner' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:plus_drk_contentelements/Resources/Public/Icons/FontAwesome/group.svg'
    ],
    'icon-plusdrkcontentelements-quote' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:plus_drk_contentelements/Resources/Public/Icons/FontAwesome/quote-left.svg'
    ],
    'icon-plusdrkcontentelements-topics-teaser' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:plus_drk_contentelements/Resources/Public/Icons/FontAwesome/th-large.svg'
    ],
    'icon-plusdrkcontentelements-trust' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:plus_drk_contentelements/Resources/Public/Icons/FontAwesome/chart-pie.svg'
    ],
    'icon-plusdrkcontentelements-video-gallery' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:plus_drk_contentelements/Resources/Public/Icons/FontAwesome/play-circle-o.svg'
    ],
    'icon-plusdrkcontentelements-menu-timeline' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:plus_drk_contentelements/Resources/Public/Icons/FontAwesome/info.svg'
        //'source' => 'EXT:core/Resources/Public/Icons/T3Icons/svgs/content/content-menu-pages.svg'
    ]
];
