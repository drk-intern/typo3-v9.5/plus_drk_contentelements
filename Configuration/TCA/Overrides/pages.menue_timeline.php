<?php
defined('TYPO3') || die();

call_user_func(function () {

    $languageFile = 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.pages.xlf:';

    // new fields for pages
    $tempColumns = [
        'tx_plusdrkcontentelements_teasershow' => [
            'exclude' => '1',
            'label' => $languageFile . 'pages.tx_plusdrkcontentelements_teasershow',
            'config' => [
                'type' => 'check',
            ],
        ],
        'tx_plusdrkcontentelements_teaserheader' => [
            'exclude' => '1',
            'label' => $languageFile . 'pages.tx_plusdrkcontentelements_teaserheader',
            'config' => [
                'type' => 'input',
                'eval' => 'trim'
            ],
        ],
        'tx_plusdrkcontentelements_teasertime' => [
            'exclude' => '1',
            'label' => $languageFile . 'pages.tx_plusdrkcontentelements_teasertime',
            'description' => $languageFile . 'pages.tx_plusdrkcontentelements_teasertime.description',
            'config' => [
                'type' => 'input',
                'eval' => 'trim'
            ],
        ],
        'tx_plusdrkcontentelements_teasertext' => [
            'exclude' => '1',
            'label' => $languageFile . 'pages.tx_plusdrkcontentelements_teasertext',
            'config' => [
                'type' => 'text',
            ],
        ],
        'tx_plusdrkcontentelements_teaserimage' => [
            'exclude' => '1',
            'label' => $languageFile . 'pages.tx_plusdrkcontentelements_teaserimage',
            'description' => $languageFile . 'pages.tx_plusdrkcontentelements_teaserimage.description',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'tx_plusdrkcontentelements_teaserimage',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' =>
                            'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'collapseAll' => true,
                    'minitems' => 0,
                    'maxitems' => 1,
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
    ];

    // add fields to TCA
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumns);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'pages',
        '--div--;' . $languageFile . 'pages.tab.teaser, tx_plusdrkcontentelements_teasershow, tx_plusdrkcontentelements_teasertime,
            tx_plusdrkcontentelements_teaserheader, tx_plusdrkcontentelements_teasertext, tx_plusdrkcontentelements_teaserimage',
        '1,4',
        ''
    );
});
