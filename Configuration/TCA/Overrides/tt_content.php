<?php
defined('TYPO3') || die();

call_user_func(static function () {

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['plusdrkcontentelements_donation_info'] = 'icon-plusdrkcontentelements-donation-info';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['plusdrkcontentelements_facts_tiles'] = 'icon-plusdrkcontentelements-facts-tiles';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['plusdrkcontentelements_icon_tiles'] = 'icon-plusdrkcontentelements-icon-tiles';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['plusdrkcontentelements_multimedia_header'] = 'icon-plusdrkcontentelements-multimedia-header';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['plusdrkcontentelements_project_partner'] = 'icon-plusdrkcontentelements-project-partner';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['plusdrkcontentelements_quote'] = 'icon-plusdrkcontentelements-quote';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['plusdrkcontentelements_topics_teaser'] = 'icon-plusdrkcontentelements-topics-teaser';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['plusdrkcontentelements_trust'] = 'icon-plusdrkcontentelements-trust';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['plusdrkcontentelements_video_gallery'] = 'icon-plusdrkcontentelements-video-gallery';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['plusdrkcontentelements_menu_timeline'] = 'tx_plusdrkcontentelements_menu_timeline';
$tempColumns = [
    'tx_plusdrkcontentelements_animation' => [
        'config' => [
            'type' => 'inline',
            'foreign_table' => 'sys_file_reference',
            'foreign_field' => 'uid_foreign',
            'foreign_sortby' => 'sorting_foreign',
            'foreign_table_field' => 'tablenames',
            'foreign_match_fields' => [
                'fieldname' => 'tx_plusdrkcontentelements_animation',
            ],
            'foreign_label' => 'uid_local',
            'foreign_selector' => 'uid_local',
            'overrideChildTca' => [
                'columns' => [
                    'uid_local' => [
                        'config' => [
                            'appearance' => [
                                'elementBrowserType' => 'file',
                                'elementBrowserAllowed' => 'svg, jpeg, jpg, png, gif',
                            ],
                        ],
                    ],
                ],
                'types' => [
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;audioOverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;videoOverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                ],
            ],
            'filter' => [
                [
                    'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                    'parameters' => [
                        'allowedFileExtensions' => 'svg, jpeg, jpg, png, gif',
                        'disallowedFileExtensions' => '',
                    ],
                ],
            ],
            'appearance' => [
                'useSortable' => 1,
                'headerThumbnail' => [
                    'field' => 'uid_local',
                    'height' => '45m',
                ],
                'enabledControls' => [
                    'info' => 1,
                    'new' => false,
                    'dragdrop' => 1,
                    'sort' => 0,
                    'hide' => 1,
                    'delete' => 1,
                    'localize' => 1,
                ],
                'elementBrowserEnabled' => 1,
                'fileUploadAllowed' => 1,
            ],
            'maxitems' => '1',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_animation',
    ],
    'tx_plusdrkcontentelements_animationtext' => [
        'config' => [
            'enableRichtext' => 1,
            'type' => 'text',
            'softref' => 'typolink_tag,email[subst],url',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_animationtext',
    ],
    'tx_plusdrkcontentelements_author_function' => [
        'config' => [
            'type' => 'input',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_author_function',
    ],
    'tx_plusdrkcontentelements_author_name' => [
        'config' => [
            'type' => 'input',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_author_name',
    ],
    'tx_plusdrkcontentelements_fact' => [
        'config' => [
            'appearance' => [
                'collapseAll' => 1,
                'enabledControls' => [
                    'delete' => 1,
                    'dragdrop' => 1,
                    'hide' => 1,
                    'info' => 1,
                    'localize' => 1,
                    'new' => 1,
                    'sort' => 1,
                ],
                'levelLinksPosition' => 'top',
                'showAllLocalizationLink' => 1,
                'showNewRecordLink' => 1,
                'showPossibleLocalizationRecords' => 1,
                'useSortable' => 1,
            ],
            'foreign_field' => 'parentid',
            'foreign_sortby' => 'sorting',
            'foreign_table' => 'tx_plusdrkcontentelements_fact',
            'foreign_table_field' => 'parenttable',
            'type' => 'inline',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_fact',
    ],
    'tx_plusdrkcontentelements_header_teaser' => [
        'config' => [
            'appearance' => [
                'collapseAll' => 1,
                'enabledControls' => [
                    'delete' => 1,
                    'dragdrop' => 1,
                    'hide' => 1,
                    'info' => 1,
                    'localize' => 1,
                    'new' => 1,
                    'sort' => 1,
                ],
                'levelLinksPosition' => 'both',
                'showAllLocalizationLink' => 1,
                'showNewRecordLink' => 1,
                'showPossibleLocalizationRecords' => 1,
                'useSortable' => 1,
            ],
            'foreign_field' => 'parentid',
            'foreign_sortby' => 'sorting',
            'foreign_table' => 'tx_plusdrkcontentelements_header_teaser',
            'foreign_table_field' => 'parenttable',
            'type' => 'inline',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_header_teaser',
    ],
    'tx_plusdrkcontentelements_image_chart' => [
        'config' => [
            'type' => 'inline',
            'foreign_table' => 'sys_file_reference',
            'foreign_field' => 'uid_foreign',
            'foreign_sortby' => 'sorting_foreign',
            'foreign_table_field' => 'tablenames',
            'foreign_match_fields' => [
                'fieldname' => 'tx_plusdrkcontentelements_image_chart',
            ],
            'foreign_label' => 'uid_local',
            'foreign_selector' => 'uid_local',
            'overrideChildTca' => [
                'columns' => [
                    'uid_local' => [
                        'config' => [
                            'appearance' => [
                                'elementBrowserType' => 'file',
                                'elementBrowserAllowed' => 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg',
                            ],
                        ],
                    ],
                ],
                'types' => [
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;audioOverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;videoOverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                ],
            ],
            'filter' => [
                [
                    'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                    'parameters' => [
                        'allowedFileExtensions' => 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg',
                        'disallowedFileExtensions' => '',
                    ],
                ],
            ],
            'appearance' => [
                'useSortable' => 1,
                'headerThumbnail' => [
                    'field' => 'uid_local',
                    'height' => '45m',
                ],
                'enabledControls' => [
                    'info' => 1,
                    'new' => false,
                    'dragdrop' => 1,
                    'sort' => 0,
                    'hide' => 1,
                    'delete' => 1,
                    'localize' => 1,
                ],
                'elementBrowserEnabled' => 1,
                'fileUploadAllowed' => 1,
            ],
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_image_chart',
    ],
    'tx_plusdrkcontentelements_partner' => [
        'config' => [
            'type' => 'inline',
            'foreign_table' => 'sys_file_reference',
            'foreign_field' => 'uid_foreign',
            'foreign_sortby' => 'sorting_foreign',
            'foreign_table_field' => 'tablenames',
            'foreign_match_fields' => [
                'fieldname' => 'tx_plusdrkcontentelements_partner',
            ],
            'foreign_label' => 'uid_local',
            'foreign_selector' => 'uid_local',
            'overrideChildTca' => [
                'columns' => [
                    'uid_local' => [
                        'config' => [
                            'appearance' => [
                                'elementBrowserType' => 'file',
                                'elementBrowserAllowed' => 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg',
                            ],
                        ],
                    ],
                ],
                'types' => [
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;audioOverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;videoOverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                ],
            ],
            'filter' => [
                [
                    'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                    'parameters' => [
                        'allowedFileExtensions' => 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg',
                        'disallowedFileExtensions' => '',
                    ],
                ],
            ],
            'appearance' => [
                'useSortable' => 1,
                'headerThumbnail' => [
                    'field' => 'uid_local',
                    'height' => '45m',
                ],
                'enabledControls' => [
                    'info' => 1,
                    'new' => false,
                    'dragdrop' => 1,
                    'sort' => 0,
                    'hide' => 1,
                    'delete' => 1,
                    'localize' => 1,
                ],
                'elementBrowserEnabled' => 1,
                'fileUploadAllowed' => 1,
            ],
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_partner',
    ],
    'tx_plusdrkcontentelements_primary' => [
        'config' => [
            'renderType' => 'inputLink',
            'type' => 'input',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_primary',
    ],
    'tx_plusdrkcontentelements_primary_label' => [
        'config' => [
            'type' => 'input',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_primary_label',
    ],
    'tx_plusdrkcontentelements_quote' => [
        'config' => [
            'type' => 'text',
            'wrap' => 'virtual',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_quote',
    ],
    'tx_plusdrkcontentelements_secondary' => [
        'config' => [
            'renderType' => 'inputLink',
            'type' => 'input',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_secondary',
    ],
    'tx_plusdrkcontentelements_secondary_label' => [
        'config' => [
            'type' => 'input',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_secondary_label',
    ],
    'tx_plusdrkcontentelements_sister' => [
        'config' => [
            'type' => 'inline',
            'foreign_table' => 'sys_file_reference',
            'foreign_field' => 'uid_foreign',
            'foreign_sortby' => 'sorting_foreign',
            'foreign_table_field' => 'tablenames',
            'foreign_match_fields' => [
                'fieldname' => 'tx_plusdrkcontentelements_sister',
            ],
            'foreign_label' => 'uid_local',
            'foreign_selector' => 'uid_local',
            'overrideChildTca' => [
                'columns' => [
                    'uid_local' => [
                        'config' => [
                            'appearance' => [
                                'elementBrowserType' => 'file',
                                'elementBrowserAllowed' => 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg',
                            ],
                        ],
                    ],
                ],
                'types' => [
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;audioOverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;videoOverlayPalette,
                                --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                    ],
                ],
            ],
            'filter' => [
                [
                    'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                    'parameters' => [
                        'allowedFileExtensions' => 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg',
                        'disallowedFileExtensions' => '',
                    ],
                ],
            ],
            'appearance' => [
                'useSortable' => 1,
                'headerThumbnail' => [
                    'field' => 'uid_local',
                    'height' => '45m',
                ],
                'enabledControls' => [
                    'info' => 1,
                    'new' => false,
                    'dragdrop' => 1,
                    'sort' => 0,
                    'hide' => 1,
                    'delete' => 1,
                    'localize' => 1,
                ],
                'elementBrowserEnabled' => 1,
                'fileUploadAllowed' => 1,
            ],
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_sister',
    ],
    'tx_plusdrkcontentelements_tile' => [
        'config' => [
            'appearance' => [
                'collapseAll' => 1,
                'enabledControls' => [
                    'delete' => 1,
                    'dragdrop' => 1,
                    'hide' => 1,
                    'info' => 1,
                    'localize' => 1,
                    'new' => 1,
                    'sort' => 1,
                ],
                'levelLinksPosition' => 'top',
                'showAllLocalizationLink' => 1,
                'showNewRecordLink' => 1,
                'showPossibleLocalizationRecords' => 1,
                'useSortable' => 1,
            ],
            'foreign_field' => 'parentid',
            'foreign_sortby' => 'sorting',
            'foreign_table' => 'tx_plusdrkcontentelements_tile',
            'foreign_table_field' => 'parenttable',
            'type' => 'inline',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_tile',
    ],
    'tx_plusdrkcontentelements_title' => [
        'config' => [
            'type' => 'input',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_title',
    ],
    'tx_plusdrkcontentelements_topic_teaser' => [
        'config' => [
            'appearance' => [
                'collapseAll' => 1,
                'enabledControls' => [
                    'delete' => 1,
                    'dragdrop' => 1,
                    'hide' => 1,
                    'info' => 1,
                    'localize' => 1,
                    'new' => 1,
                    'sort' => 1,
                ],
                'levelLinksPosition' => 'top',
                'showAllLocalizationLink' => 1,
                'showNewRecordLink' => 1,
                'showPossibleLocalizationRecords' => 1,
                'useSortable' => 1,
            ],
            'foreign_field' => 'parentid',
            'foreign_sortby' => 'sorting',
            'foreign_table' => 'tx_plusdrkcontentelements_topic_teaser',
            'foreign_table_field' => 'parenttable',
            'type' => 'inline',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_topic_teaser',
    ],
    'tx_plusdrkcontentelements_videos' => [
        'config' => [
            'appearance' => [
                'collapseAll' => 1,
                'enabledControls' => [
                    'delete' => 1,
                    'dragdrop' => 1,
                    'hide' => 1,
                    'info' => 1,
                    'localize' => 1,
                    'new' => 1,
                    'sort' => 1,
                ],
                'levelLinksPosition' => 'both',
                'showAllLocalizationLink' => 1,
                'showNewRecordLink' => 1,
                'showPossibleLocalizationRecords' => 1,
                'useSortable' => 1,
            ],
            'foreign_field' => 'parentid',
            'foreign_sortby' => 'sorting',
            'foreign_table' => 'tx_plusdrkcontentelements_videos',
            'foreign_table_field' => 'parenttable',
            'type' => 'inline',
        ],
        'exclude' => 1,
        'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_plusdrkcontentelements_videos',
    ],
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.div._plusdrkcontentelements_',
    '--div--',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.plusdrkcontentelements_donation_info',
    'value' => 'plusdrkcontentelements_donation_info',
    'icon' => 'icon-plusdrkcontentelements-donation-info',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.plusdrkcontentelements_facts_tiles',
    'value' => 'plusdrkcontentelements_facts_tiles',
    'icon' => 'icon-plusdrkcontentelements-facts-tiles',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.plusdrkcontentelements_icon_tiles',
    'value' => 'plusdrkcontentelements_icon_tiles',
    'icon' => 'icon-plusdrkcontentelements-icon-tiles',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.plusdrkcontentelements_multimedia_header',
    'value' => 'plusdrkcontentelements_multimedia_header',
    'icon' => 'icon-plusdrkcontentelements-multimedia-header',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.plusdrkcontentelements_project_partner',
    'value' => 'plusdrkcontentelements_project_partner',
    'icon' => 'icon-plusdrkcontentelements-project-partner',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.plusdrkcontentelements_quote',
    'value' => 'plusdrkcontentelements_quote',
    'icon' => 'icon-plusdrkcontentelements-quote',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.plusdrkcontentelements_topics_teaser',
    'value' => 'plusdrkcontentelements_topics_teaser',
    'icon' => 'icon-plusdrkcontentelements-topics-teaser',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.plusdrkcontentelements_trust',
    'value' => 'plusdrkcontentelements_trust',
    'icon' => 'icon-plusdrkcontentelements-trust',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.plusdrkcontentelements_video_gallery',
    'value' => 'plusdrkcontentelements_video_gallery',
    'icon' => 'icon-plusdrkcontentelements-video-gallery',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'label' => 'LLL:EXT:plus_drk_contentelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.plusdrkcontentelements_menu_timeline',
    'value' => 'plusdrkcontentelements_menu_timeline',
    'icon' => 'icon-plusdrkcontentelements_menu_timeline',
];
$tempTypes = [
    'plusdrkcontentelements_donation_info' => [
        'columnsOverrides' => [
            'tx_plusdrkcontentelements_animation' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_donation_info.xlf:animation',
            ],
            'tx_plusdrkcontentelements_animationtext' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_donation_info.xlf:animation.text',
            ],
            'bodytext' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_donation_info.xlf:text',
                'config' => [
                    'enableRichtext' => 1,
                ],
            ],
            'image' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_donation_info.xlf:logo',
            ],
        ],
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_plusdrkcontentelements_animation,tx_plusdrkcontentelements_animationtext,bodytext,image,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, tx_gridelements_container, tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
    ],
    'plusdrkcontentelements_facts_tiles' => [
        'columnsOverrides' => [
            'tx_plusdrkcontentelements_fact' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_facts_tiles.xlf:tt_content.fact',
            ],
            'bodytext' => [
                'config' => [
                    'enableRichtext' => 1,
                ],
            ],
        ],
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,bodytext,tx_plusdrkcontentelements_fact,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, tx_gridelements_container, tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
    ],
    'plusdrkcontentelements_icon_tiles' => [
        'columnsOverrides' => [
            'tx_plusdrkcontentelements_tile' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_icon_tiles.xlf:icon_tiles.tile.label',
            ],
        ],
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_plusdrkcontentelements_tile,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, tx_gridelements_container, tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
    ],
    'plusdrkcontentelements_multimedia_header' => [
        'columnsOverrides' => [
            'assets' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_multimedia_header.xlf:multimedia_header.title',
            ],
            'tx_plusdrkcontentelements_primary' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_multimedia_header.xlf:multimedia_header.primary.label',
            ],
            'tx_plusdrkcontentelements_primary_label' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_multimedia_header.xlf:multimedia_header.primary_label.label',
            ],
            'tx_plusdrkcontentelements_secondary' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_multimedia_header.xlf:multimedia_header.secondary.label',
            ],
            'tx_plusdrkcontentelements_secondary_label' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_multimedia_header.xlf:multimedia_header.secondary_label.label',
            ],
            'tx_plusdrkcontentelements_header_teaser' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_multimedia_header.xlf:multimedia_header.header_teaser.label',
            ],
            'bodytext' => [
                'config' => [
                    'enableRichtext' => 1,
                ],
            ],
        ],
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,assets,bodytext,tx_plusdrkcontentelements_primary,tx_plusdrkcontentelements_primary_label,tx_plusdrkcontentelements_secondary,tx_plusdrkcontentelements_secondary_label,tx_plusdrkcontentelements_header_teaser,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, tx_gridelements_container, tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
    ],
    'plusdrkcontentelements_project_partner' => [
        'columnsOverrides' => [
            'tx_plusdrkcontentelements_sister' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_project_partner.xlf:project_partner.sister.label',
            ],
            'tx_plusdrkcontentelements_partner' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_project_partner.xlf:project_partner.partner.label',
            ],
        ],
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_plusdrkcontentelements_sister,tx_plusdrkcontentelements_partner,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, tx_gridelements_container, tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
    ],
    'plusdrkcontentelements_quote' => [
        'columnsOverrides' => [
            'tx_plusdrkcontentelements_quote' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_quote.xlf:tt_content.tx_plusdrkcontentelements_quote.label',
            ],
            'tx_plusdrkcontentelements_author_name' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_quote.xlf:tt_content.tx_plusdrkcontentelements_author_name.label',
            ],
            'tx_plusdrkcontentelements_author_function' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_quote.xlf:tt_content.tx_plusdrkcontentelements_author_function.label',
            ],
            'image' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_quote.xlf:tt_content.image.label',
            ],
        ],
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_plusdrkcontentelements_quote,tx_plusdrkcontentelements_author_name,tx_plusdrkcontentelements_author_function,image,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, tx_gridelements_container, tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
    ],
    'plusdrkcontentelements_topics_teaser' => [
        'columnsOverrides' => [
            'tx_plusdrkcontentelements_topic_teaser' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_topics_teaser.xlf:topic',
            ],
        ],
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_plusdrkcontentelements_topic_teaser,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, tx_gridelements_container, tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
    ],
    'plusdrkcontentelements_trust' => [
        'columnsOverrides' => [
            'image' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_trust.xlf:trust.image.label',
            ],
            'tx_plusdrkcontentelements_title' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_trust.xlf:trust.title.label',
            ],
            'tx_plusdrkcontentelements_image_chart' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_trust.xlf:trust.image-chart.label',
            ],
            'bodytext' => [
                'config' => [
                    'enableRichtext' => 1,
                ],
            ],
        ],
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,image,tx_plusdrkcontentelements_title,bodytext,tx_plusdrkcontentelements_image_chart,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, tx_gridelements_container, tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
    ],
    'plusdrkcontentelements_video_gallery' => [
        'columnsOverrides' => [
            'tx_plusdrkcontentelements_videos' => [
                'label' => 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be_video_gallery.xlf:video_gallery.videos.label',
            ],
            'bodytext' => [
                'config' => [
                    'enableRichtext' => 1,
                ],
            ],
        ],
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,bodytext,tx_plusdrkcontentelements_videos,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, tx_gridelements_container, tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
    ],
    'plusdrkcontentelements_menu_timeline' => [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;;general,
                --palette--;;headers,
                pages;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:pages.ALT.menu_formlabel,
            --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;language,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                --palette--;;hidden,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
            --div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended, tx_gridelements_container, tx_gridelements_columns,
            --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
        ],
];
$GLOBALS['TCA']['tt_content']['types'] += $tempTypes;
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'plus_drk_contentelements',
    'Configuration/TypoScript/',
    'plus_drk_contentelements'
);

});

