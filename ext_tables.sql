CREATE TABLE tt_content (
    tx_plusdrkcontentelements_animation int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_animationtext mediumtext,
    tx_plusdrkcontentelements_author_function varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_author_name varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_fact int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_header_teaser int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_image_chart int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_partner int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_primary varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_primary_label varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_quote mediumtext,
    tx_plusdrkcontentelements_secondary varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_secondary_label varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_sister int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_tile int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_title varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_topic_teaser int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_videos int(11) unsigned DEFAULT '0' NOT NULL
);
CREATE TABLE tx_plusdrkcontentelements_fact (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    t3ver_count int(11) DEFAULT '0' NOT NULL,
    t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
    t3ver_move_id int(11) DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_prefix varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_text mediumtext,
    tx_plusdrkcontentelements_suffix varchar(255) DEFAULT '' NOT NULL,
    KEY language (l10n_parent,sys_language_uid)
);
CREATE TABLE tx_plusdrkcontentelements_header_teaser (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    t3ver_count int(11) DEFAULT '0' NOT NULL,
    t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
    t3ver_move_id int(11) DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_image int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_title varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_description mediumtext,
    tx_plusdrkcontentelements_link varchar(255) DEFAULT '' NOT NULL,
    KEY language (l10n_parent,sys_language_uid)
);
CREATE TABLE tx_plusdrkcontentelements_tile (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    t3ver_count int(11) DEFAULT '0' NOT NULL,
    t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
    t3ver_move_id int(11) DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_layout varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_link varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_topic varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_description mediumtext,
    tx_plusdrkcontentelements_icon int(11) unsigned DEFAULT '0' NOT NULL,
    KEY language (l10n_parent,sys_language_uid)
);
CREATE TABLE tx_plusdrkcontentelements_topic_teaser (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    t3ver_count int(11) DEFAULT '0' NOT NULL,
    t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
    t3ver_move_id int(11) DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_button_donate_primary int(11) DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_link_primary varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_link_text_primary varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_look varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_button_donate_secondary int(11) DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_image int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_link_secondary varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_link_text_secondary varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_title varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_title_type varchar(255) DEFAULT '' NOT NULL,
    tx_plusdrkcontentelements_text mediumtext,
    tx_plusdrkcontentelements_text_color varchar(255) DEFAULT '' NOT NULL,
    KEY language (l10n_parent,sys_language_uid)
);
CREATE TABLE tx_plusdrkcontentelements_videos (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    t3ver_count int(11) DEFAULT '0' NOT NULL,
    t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
    t3ver_move_id int(11) DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_video int(11) unsigned DEFAULT '0' NOT NULL,
    tx_plusdrkcontentelements_preview_image int(11) unsigned DEFAULT '0' NOT NULL,
    KEY language (l10n_parent,sys_language_uid)
);

CREATE TABLE pages (
   tx_plusdrkcontentelements_teasershow smallint(5) unsigned DEFAULT '0' NOT NULL,
   tx_plusdrkcontentelements_teasertime varchar(255) DEFAULT '' NOT NULL,
   tx_plusdrkcontentelements_teaserheader varchar(255) DEFAULT '' NOT NULL,
   tx_plusdrkcontentelements_teasertext text,
   tx_plusdrkcontentelements_teaserimage int(11) unsigned DEFAULT '0' NOT NULL
);
