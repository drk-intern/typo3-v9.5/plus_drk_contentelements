<?php
defined('TYPO3') || die();

call_user_func(static function () {

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_plusdrkcontentelements_fact, tx_plusdrkcontentelements_header_teaser, tx_plusdrkcontentelements_tile, tx_plusdrkcontentelements_topic_teaser, tx_plusdrkcontentelements_videos');

});

